## 2.0.1

- Minor correction
- Deprecation note

## 2.0.0

- Removed the `Xcvbnm` class, and replaced it with a top level function.

## 1.0.1

- Fix analysis hints

## 1.0.0

- Slight cleanup and 1.0.0 release

## 0.5.7

- Remove all implicit cast issues

## 0.5.6

- Fixed all dart 2.0 errors and warnings

## 0.5.5

- Started Changelog
- Made xcvbnm strong-mode compliant