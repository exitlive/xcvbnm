# xcvbnm

> **This package is deprecated.** Because there are inconsistencies with the
> score result compared to the original library, and I don't have the time to
> investigate them, I recommend you use the package
> [https://pub.dev/packages/zxcvbn](`zxcvbn`) instead.

* * *

This is a dart port of the great [dropbox/zxcvbn](https://github.com/dropbox/zxcvbn) library, a realistic password strength 
estimator.

This library can be used on the server and in the browser.

Keep in mind that the dictionary is quite big, so using it in the browser comes
with a substantial increase of the script size.

**Please note, that library did not port all functionality but is already used in production.**

## Usage

```dart
import 'package:xcvbnm/xcvbnm.dart' as xcvbnm;

main() {
  final password = "my_pwd_1";
  final result = xcvbnm.estimate(password);
  print(result.toMap());
}
```


## Demo

```bash
$  dart example/bin/xcvbnm.dart --help
  -h, --help     Usage help
  -m, --match    Max number max of match to display
                 (defaults to "4")
$ dart example/bin/xcvbnm.dart
$ dart example/bin/xcvbnm.dart 9IOksjdopwd
$ dart example/bin/xcvbnm.dart "my password"
$ dart example/bin/xcvbnm.dart pwd1 pwd2 -m 1
```

## Development

### Guidelines and format

Before each commit format at the root of the project

```bash
$ dartfmt -w .
```
    
### Test

Run all tests in multiple platforms

```bash
$ pub run test
```
    
