library xcvbnm.feedback;

import 'package:xcvbnm/src/scoring.dart' as scoring;

class Feedback {
  static final String generalSuggestion =
      'Use a few words, avoid common phrases.';
  static final String spatialSingleTurnSuggestion =
      'Straight rows of keys are easy to guess. '
      'Use a longer keyboard pattern with more turns.';
  static final String spatialMultiTurnSuggestion =
      'Short keyboard patterns are easy to guess. '
      'Use a longer keyboard pattern with more turns.';
  static final String repeatSingleCharSuggestion =
      'Repeats like \'aaa\' are easy to guess. '
      'Avoid repeated words and characters.';
  static final String repeatMultiCharSuggestion =
      'Repeats like \'abcabcabc\' are only slightly harder to guess than \'abc\'. '
      'Avoid repeated words and characters.';
  static final String sequenceSuggestion =
      'Sequences like abc or 6543 are easy to guess. '
      'Avoid sequences.';
  static final String recentYearSuggestion = 'Recent years are easy to guess. '
      'Avoid recent years and years that are associated with you.';
  static final String dateSuggestion = 'Dates are often easy to guess. '
      'Avoid dates and years that are associated with you.';
  static final String top10PasswordSuggestion =
      'This is a top-10 common password.';
  static final String top100PasswordSuggestion =
      'This is a top-100 common password.';
  static final String commonPasswordSuggestion =
      'This is a very common password.';
  static final String englishWordSuggestion =
      'A word by itself is easy to guess.';
  static final String nameSuggestion =
      'Common names and surnames are easy to guess.';

  String suggestion = generalSuggestion;

  Feedback(int score, List<scoring.Match> sequence) {
    var sequenceCopy = <scoring.Match>[];
    sequenceCopy.addAll(sequence);
    getFeedback(score, sequenceCopy);
  }

  void getFeedback(int score, List<scoring.Match> sequence) {
    // starting feedback
    sequence.removeWhere((match) => match.token == null);
    if (sequence?.isEmpty ?? false) return;

    // no feedback if score is good or great.
    if (score > 2) {
      suggestion = '';
      return;
    }

    // tie feedback to the longest match for longer sequences
    var longestMatch = sequence.first;
    for (var match in sequence) {
      if (match.token.length > longestMatch.token.length) longestMatch = match;
    }
    getMatchFeedback(longestMatch, sequence.length == 1);
  }

  void getMatchFeedback(scoring.Match match, bool isSoleMatch) {
    if (match is scoring.DictionaryMatch) {
      getDictionaryMatchFeedback(match, isSoleMatch);
    } else if (match is scoring.SpatialMatch) {
      if (match.turns == 1) {
        suggestion = spatialSingleTurnSuggestion;
      } else {
        suggestion = spatialMultiTurnSuggestion;
      }
    } else if (match is scoring.RepeatMatch) {
      if (match.baseToken.length == 1) {
        suggestion = repeatSingleCharSuggestion;
      } else {
        suggestion = repeatMultiCharSuggestion;
      }
    } else if (match is scoring.SequenceMatch) {
      suggestion = sequenceSuggestion;
    } else if (match is scoring.RegexMatch) {
      if (match.regexName == 'recent_year') suggestion = recentYearSuggestion;
    } else if (match is scoring.DateMatch) {
      suggestion = dateSuggestion;
    }
  }

  void getDictionaryMatchFeedback(
      scoring.DictionaryMatch match, bool isSoleMatch) {
    bool l33t = match.l33t == true;
    bool reversed = match.reversed == true;
    if (match.dictionaryName == 'passwords') {
      if (isSoleMatch && !l33t && !reversed) {
        if (match.rank <= 10) {
          suggestion = top10PasswordSuggestion;
        } else if (match.rank <= 100) {
          suggestion = top100PasswordSuggestion;
        } else {
          suggestion = commonPasswordSuggestion;
        }
      }
    } else if (match.dictionaryName == 'english') {
      if (isSoleMatch) suggestion = englishWordSuggestion;
    } else if (['surnames', 'male_names', 'female_names']
        .contains(match.dictionaryName)) {
      suggestion = nameSuggestion;
    }
  }

  @override
  String toString() {
    return suggestion;
  }
}
