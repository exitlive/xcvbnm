import 'package:xcvbnm/xcvbnm.dart' as xcvbnm;

void main() {
  final password = 'my_pwd_1';
  final result = xcvbnm.estimate(password);

  if (result.score < 3) {
    print('Password not strong enough. It can be cracked in '
        '${result.crackTimeDisplay}. ${result.feedback}');
  }
}
