library xcvbnm.browser.demo;

import 'package:xcvbnm/xcvbnm.dart' as xcvbnm;
import 'package:xcvbnm/src/scoring.dart' as scoring;
import 'dart:html';

void main() {
  int maxMatchToDisplay;

  // good sample of data
  final passwords = [
    '',
    'zxcvbn',
    'qwER43@!',
    'Tr0ub4dour&3',
    'correcthorsebatterystaple',
    'coRrecth0rseba++ery9.23.2007staple\$',
    'D0g..................',
    'abcdefghijk987654321',
    'neverforget13/3/1997',
    '1qaz2wsx3edc',
    'temppass22',
    'briansmith',
    'briansmith4mayor',
    'password1',
    'viking',
    'thx1138',
    'ScoRpi0ns',
    'do you know',
    'ryanhunter2000',
    'rianhunter2000',
    'asdfghju7654rewq',
    'AOEUIDHG&*()LS_',
    '12345678',
    'defghi6789',
    'rosebud',
    'Rosebud',
    'ROSEBUD',
    'rosebuD',
    'ros3bud99',
    'r0s3bud99',
    'R0\$38uD99',
    'verlineVANDERMARK',
    'eheuczkqyq',
    'rWibMFACxAUGZmxhVncy',
    'Ba9ZyWABu99[BK#6MBgbH88Tofv)vs\$w'
  ];

  DivElement getResultEl(String password) {
    // The only call
    final result = xcvbnm.estimate(password);

    DivElement resultEl = DivElement()..classes.add('app-result');
    List<String> lines = [
      '----- Result ------',
      'password:           ${result.password}',
      'entropy:            ${result.entropy}',
      'crack_time:         ${Duration(milliseconds: result.crackTime)}',
      'crack_time_display: ${result.crackTimeDisplay}',
      'score from 0 to 4:  ${result.score}',
      'cecl_time:          ${Duration(milliseconds: result.calcTime)}'
    ];
    for (String line in lines) {
      resultEl.append(PreElement()
        ..classes.add('app-line')
        ..text = line);
    }

    print(result.toJson());
    if (result.matchSequence != null && result.matchSequence.isNotEmpty) {
      int i = 1;
      DivElement matchesEl = DivElement()..classes.add('app-matches');
      resultEl.append(matchesEl);
      for (var match in result.matchSequence) {
        if (maxMatchToDisplay != null && i > maxMatchToDisplay) {
          break;
        }

        DivElement matchEl = DivElement()..classes.add('app-match');
        matchesEl.append(matchEl);
        List<String> lines = [
          '----- match ${i++}/${result.matchSequence.length}'
        ];
        if (match is scoring.Match) {
          lines.add('\'${match.token}\'');
          lines.add('pattern:       \'${match.pattern}\'');
          lines.add('entropy:       ${match.entropy}');

          if (match.baseEntropy != null) {
            lines.add('base_entropy:  ${match.baseEntropy}');
          }

          if (match is scoring.DictionaryMatch) {
            if (match.rank != null) {
              lines.add('rank:          ${match.rank}');
            }
            if (match.uppercaseEntropy != null) {
              lines.add('upper_entropy: ${match.uppercaseEntropy}');
            }
          }
        }
        for (String line in lines) {
          matchEl.append(PreElement()
            ..classes.add('app-line')
            ..text = line);
        }
      }
    }
    return resultEl;
  }

  DivElement resultsEl = DivElement()..classes.add('app-results');
  for (String password in passwords) {
    DivElement resultEl = getResultEl(password);
    print(resultEl);
    resultsEl.append(resultEl);
  }
  DivElement passwordResultsEl = DivElement()..classes.add('app-results');
  querySelector('#app_password_content').append(passwordResultsEl);
  InputElement appPasswordEl = querySelector('#app_password') as InputElement;
  appPasswordEl.onInput.listen((e) {
    String password = appPasswordEl.value;
    print(password);
    DivElement resultEl = getResultEl(password);
    passwordResultsEl
      ..children.clear()
      ..append(resultEl);
  });

  querySelector('#app_content')
    ..children.clear()
    ..append(resultsEl);
}
